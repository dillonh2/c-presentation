#include <stdlib.h>
#include <stdio.h>

static int array[1024*1024*1024];

void func(int** p)
{
	int z = 6;
	printf("func: z is %d\n", z);
	*p = &z;
}

int main(int argc, char** argv)
{
	// Stack
	int x = 1;
	int* ptr_stack = &x;
	printf("ptr_stack = %p, *ptr_stack = %d\n", ptr_stack, *ptr_stack);
	printf("\n");	
	// Heap
	int* ptr_heap = malloc(sizeof(int));
	*ptr_heap = 1;
	printf("ptr_heap = %p, ptr_stack - ptr_heap = %d, *ptr_heap = %d\n", ptr_heap, (int)((char*)ptr_stack - (char*)ptr_heap), *ptr_heap);
	free(ptr_heap);
	printf("\n");	
	// Another function's stack
	int q = 2;
	int* ptr_func = NULL;
	func(&ptr_func);
	printf("ptr_func = %p, &q = %p, &q - ptr_func = %d, *ptr_func = %d\n", ptr_func, &q, (int)((char*)&q - (char*)ptr_func), q);
    array[123456] = 78;
	return 0;
}
