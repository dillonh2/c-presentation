#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

struct collection {
    int32_t z;      // 0x0
    int32_t y;      // 0x4
    int16_t x;      // 0x8
};

struct collection_packed {
    int32_t z;      // 0x0
    int16_t x;      // 0x4
    int32_t y;      // 0x6
} __attribute__((packed));


int main(int argc, char** argv)
{
    struct collection c = {
        .x = 5,
        .y = 6
    };
    struct collection c2 = {};
    struct collection* cptr = &c;
    c2.x = 7;
    printf("%zu\n", sizeof(struct collection));
    printf("%zu\n", sizeof(struct collection_packed));
	return 0;
}
