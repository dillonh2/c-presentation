#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

int main(int argc, char** argv)
{
    printf("sizeof(float)     = %zu\n", sizeof(float));
    printf("sizeof(double)    = %zu\n", sizeof(double));
    printf("sizeof(char)      = %zu\n", sizeof(char));
    printf("sizeof(short)     = %zu\n", sizeof(short));
    printf("sizeof(int)       = %zu\n", sizeof(int));
    printf("sizeof(long)      = %zu\n", sizeof(long));
    printf("sizeof(long long) = %zu\n", sizeof(long long));
    printf("sizeof(uint8_t)    = %zu\n", sizeof(uint8_t));
    uint8_t x = 255;
    printf("x  = %0x\n", (int8_t)x);
    int8_t y = (int8_t)x;
    printf("y  = %0x\n", (int8_t)y);
    //printf("sizeof(int8_t)    = %zu\n", sizeof(int8_t));
    //printf("sizeof(int16_t)   = %zu\n", sizeof(int16_t));
    //printf("sizeof(int32_t)   = %zu\n", sizeof(int32_t));
    printf("sizeof(int64_t)   = %zu\n", sizeof(int64_t));
	return 0;
}
